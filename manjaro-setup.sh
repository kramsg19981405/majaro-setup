#!/bin/bash
while true
do
clear
echo "=============================="
echo "welcome to my setup script for arch"
echo "=============================="
echo "enter 1 to change and synchronise mirrors 1: "
echo "=============================="
echo "enter 2 to check for updates 2: "
echo "=============================="
echo "enter 3 for vmware tools install 3. "
echo "enter 4 for generating rsa keys 4. "
echo "ienter 5 nstall PIA and qbittorrent 5. " 
echo "enter q to quit menu q: "
echo  -e "\n "
echo  -e "enter your choice \c"
read  answer
case "$answer" in
1) cd /etc/pacman.d/
     rm mirrorlist
     echo  ...............pulling latest Australian Mirrorors 
    wget -o mirrorlist https://gitlab.com/kramsg19981405/majaro-setup/-/raw/master/mirrorlist.txt
    mv mirrorlist.txt mirrorlist
    echo ................changing mirrors 
    sudo pacman -Syy ;;
2) sudo pacman -Syu ;;
3) git clone https://github.com/rasa/vmware-tools-patches.git
    cd vmware-tools-patches
    sudo ./patched-open-vm-tools.sh
    sudo pacman -S asp
    asp checkout open-vm-tools
   cd open-vmtools/repos/community-x86_64/
   makepkg -s --asdeps
   sudo cp vm* /usr/lib/systemd/system
   sudo systemctl enable vmware-vmblock-fuse
   sudo systemctl enable vmtoolsd ;;
4) sudo pacman -S openssh
    sudo systemctl start sshd
    ssh-keygen -b 4096 ;;
5) sudo pacman -S yay
     yay -S private-internet-access-vpn qbittorrent ;;
q) exit ;;
esac
echo -e "enter to return to main menu \c"
read input 
done






